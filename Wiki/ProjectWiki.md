If the project is about new algorithm,web-services or APIs than using public repository for it development it will leave comprehensive information online for all to see. Significantly reducing the competitive advantage in the commercial market your new project provides.

One should use private repository for closed-source application especially if the given project is develop with monetary benefits in mind. Copyright and licenses can protect your work legally, but it is always possible for some other entity to use or steal source code without your permission. Under such scenario it's up to you to find the culprit, be able to prove that the source code belong to your application before the judge who will declare the verdict in your law suite.

User security is a critical topic, any mishap made in code which is related to user information and credentials will be easily found and exploited by attackers if they have access to source code.

Using repository hosting service such as BitBucket,GitHub, etc. for development of a project is ideal choice when the team is divided geographically and are in different time zones. 

The use of private repository  at repository hosting service such as bitbucket solve two major issues the team may face first it. First it provide some security against unauthorized access and second it allow team to actively manage the project progress without being bound to time or location and provide greater version control that is which changes will be done to project.